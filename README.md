# SSE Promotion Code #

* Nominee Name: Muhammad Yasir
* Project Name/s:
	* Tropic Skincare Fourfront Integration
	* Tropic Skincare Marco Integration
* Language/s: JavaScript
* Role: Software Engineer
* Lead Engineer/s (in Project):
	* Shuja Zaka Khan
* Project Manager/s:
	* Junaid Altaf
* Allocation: 100% On both Projects

## Projects ##

### Tropic Skincare Fourfront Integration ###

* Project Details:
	* In this project we have connected Fourfront external system with NetSuite. In such a way that if some
	  transactional operation is done in Fourfront System it will be synced to NetSuite. Transactional Flows 
	  Include: Sales Order Import, Invoice Import, Sales Return Import, Credit Memo Import, Journal Entries Import. 
	  Other Flows Include: Inventory Export, etc.
* Project Plan of Processing:
	* Import Flow:
		* We have create a middleware Node JS application which run's periodically and reads any new file created by
	  	  Fourfront on SFTP.
		* Middleware application then reads the file and preprocess it to perform all the necessary operations (Discount distributions,
	      orders splitting, etc) on data and send data to NetSuite using Restlet API endpoint.
		* In NetSuite we route the request based on action provided by Middleware to specific module.
		* This module performs all the operations required (Search Items in NS, Search/Create Customer in NS, etc) to make
	      data ready for record creation.
		* After all the processing it creates the record in NetSuite.
		* In case if the data provided is too much to be processed with in Restlet time limit. We stop the processing at 180 seconds
	      threshold and push all the data to a Queue (Custom Record) in NetSuite with pending state.
		* Later on a Map Reduce script is scheduled to process these pending records.
	* Export Flow:
		* Middleware Application request Restlet API Endpoint to export the requested data.
		* Restlet routes the request to specific module based on action provided by middleware.
		* Module searches for any new record created after last time the sync opetation ran.
		* If there are any records created/edited it processes the data to match external system format
		* Module returns the data to middleware.
		* Middleware creates a CSV file on Specified SFTP.
* Project Files Included:
	* SalesOrder.js
	* Customer.js
	* Item.js
	* f3_create_zero_order_ue.js
	* f3_populate_cust_fields_ue.js
	* f3_populate_item_accounts_ue.js
	* f3_shipping_method_duplicate_handing_ue.js
	* f3_invoice_pending_orders_mr.js
	* f3_copy_order_zero_cs.js
* Project Folder Structure:
	* Folder Structure consist of different folders for each script type. For Example: user_events for User Events Script Type, etc.
	* Modules folder is for the code files which are not of any NetSuite script type but are invoked for each specific operational Flow.
	  For example: For Sales Order Import SalesOrder.js module is used.
* Project Files Naming Convention:
	* Modules code files are named specifically to each flow or operation. For Example: SalesOrder.js is for Sales Order Import,
	  Customer.js is for Customer related operations.
	* Any NetSuite Script Type has a naming convention starting from **f3**. Which segregates our code files from others.
	* Each NetSuite Script Type file is ending with their type. For Example: A user event script contains **ue** at end of its name.
* Project/Code files limitations:
	* All the code related limitations (specific nested loops usage, etc) are mentioned with in the code file in comments.

### Tropic Skincare Marco Integration ###

* Project Details:
	* In this project we have connected Marco external system with NetSuite. In such a way that if some
	  transactional operation is done in Marco System it will be synced to NetSuite. Transactional Flows 
	  Include: Work Order Export, Work Order Issue Component Import, Inventory Adjustment Export, Item Receipt Export,
	  Inventory Transfer Export. 
	  Other Flows Include: Inventory Export, etc.
* Project Plan of Processing:
	* Import Flow:
		* We have create a middleware Node JS application which run's periodically and reads any new file created by
	  	  Marco on SFTP.
		* Middleware application then reads the file and preprocess it to perform all the necessary operations (mapping data
		  according to NetSuite, etc) on data and send data to NetSuite using Restlet API endpoint.
		* In NetSuite we route the request based on action provided by Middleware to specific module.
		* This module performs all the operations required (Search Items in NS, etc) to make data ready for record creation.
		* After all the processing it creates the record in NetSuite.
		* In case if the data provided is too much to be processed with in Restlet time limit. We stop the processing at 
		  180 seconds threshold and push all the data to a Queue (Custom Record) in NetSuite with pending state.
		* Later on a Map Reduce script is scheduled to process these pending records.
	* Export Flow:
		* Middleware Application request Restlet API Endpoint to export the requested data.
		* Restlet routes the request to specific module based on action provided by middleware.
		* Module searches for any new record created after last time the sync opetation ran.
		* If there are any records created/edited it processes the data to match external system format
		* Module returns the data to middleware.
		* Middleware creates a CSV file on Specified SFTP.
* Project Files Included:
	* InventoryAdjustment.js
	* WoIssueComponent.js
	* WorkOrders.js
* Project Folder Structure:
	* Folder Structure consist of different folders for each script type. For Example: user_events for User Events Script Type, etc.
	* Modules folder is for the code files which are not of any NetSuite script type but are invoked for each specific operational Flow.
	  For example: For Work Order Export WorkOrder.js module is used.
* Project Files Naming Convention:
	* Modules code files are named specifically to each flow or operation. For Example: WorkOrder.js is for Work Order Export, etc.
	* Any NetSuite Script Type has a naming convention starting from **f3**. Which segregates our code files from others.
	* Each NetSuite Script Type file is ending with their type. For Example: A user event script contains **ue** at end of its name.
* Project/Code files limitations:
	* All the code related limitations (specific nested loops usage, etc) are mentioned with in the code file in comments.

## Code Editor Details ##
* Editor Used: Visual Studio Code
* Spaces: 2
* End of Line Sequence: LF
* Script max line length in 600 lines
* Function max line length is 70 lines
* Editor Column Width Setting is 100 characters

## Exceptional Handling:
* Exception Handling: I have logged all the erros in catch block. The single entry point function of script is responsible
  for logging the error. In some cases other functions also contain try catch block where it was required.
	* In some cases the error is logged using just script log.error. The reason behind this is that in those operation
	  it was not trivial to stop the flow in some case of error.
	* Other cases include an Error Module which logs the error in a Custom Record in NetSuite and stops the flow. This
	  error is then emailed to Client using a Saved Search so that the client can correct it.
	* Exceptional handling is being done on entry-point scripts/modules. The inner dependent modules/classes/libraries 
	  has no exceptional handling except some places where it was required.
* In both the project we identify the severity of task and use try catch accordingly otherwise we use main try catch block 
       for error handling.
* For Error Handling we have a custom record(table) in NetSuite. This custom record "Fourfront Error logs" and "Marco Error Logs" 
       records all the failure that a client must see and fix those errors.