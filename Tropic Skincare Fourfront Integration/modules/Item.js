'use strict';

/**
 *
 * This file is responsible to search items in NetSuite
 *
 * Version    Date                    Author           Remarks
 * 1.0.0      2020-11-03              Yasir
 *
 */

import _ from '../lib/lodash';

import NSCrud from '../lib/f3_nscrud_base';
import globalConstants from '../globalConstants';
import LogHandler from './LogHandler';
import ErrorLog from './ErrorLog';

const {
  COMMON,
  ITEM,
} = globalConstants;

let InventoryEntity;
let syncFlow;
let SEARCH_OPERATOR;

/**
 *
 * @param {Object} context
 * @returns {Array}
 */
const executeSearch = (context = {}) => {
  try {
    const itemSearch = InventoryEntity.search(context);
    const itemRecords = InventoryEntity.getAllRecords(itemSearch);
    const itemParsedRecords = InventoryEntity.parseSearchResults(itemRecords);
    return itemParsedRecords;
  } catch (error) {
    // Logging ending detials of script and throwing Error
    LogHandler.commit();
    ErrorLog.searchNotExecuted({
      messageDetails: error.name, recordType: COMMON.ITEM_TYPE, syncFlow,
    });
  }
};

/**
 * Create Column and Filter for Item Saved Search
 * @param {Array} itemNumbers
 * @returns {Array}
 */
const getItem = (itemNumbers = []) => {
  InventoryEntity.columns = [
    { name: COMMON.INTERNAL_ID, type: COMMON.ByValue },
    { name: ITEM.SKU_CODE, type: COMMON.ByValue },
  ];

  // Faltenning the Array of Array containing items and creating filter dynamically
  // Flat map returns 3 arguments the value, current index and collection Array. We check if
  // the Array length has reached final item or not and append OR filter else return just filter.
  const inventoryFilter = _.flatMap(itemNumbers, (value, index, Array) => {
    const itemFilter = [ITEM.ITEM_ID, SEARCH_OPERATOR.IS, value];
    return Array.length - 1 !== index ? [itemFilter, 'OR'] : [itemFilter];
  });

  // Appending Item's filter with remaining filters
  const filters = [
    [COMMON.IS_INACTIVE, SEARCH_OPERATOR.IS, COMMON.IS_FALSE],
    'AND',
    [...inventoryFilter],
  ];

  return _.map(executeSearch({ filters }), (value) => ({
    [ITEM.SKU_CODE]: value[ITEM.SKU_CODE],
    [COMMON.INTERNAL_ID]: value[COMMON.INTERNAL_ID],
  }));
};

/**
 * Initializing NSCrud Object
 * @param {Object} BaseModule
 */
const init = (BaseModule) => {
  InventoryEntity = BaseModule;
  InventoryEntity.recordType = COMMON.ITEM;
  syncFlow = 'IMPORT';
  SEARCH_OPERATOR = InventoryEntity.getSearchOperator();

  return {
    getItem,
    executeSearch,
  };
};

export default init(new NSCrud.Entity());
