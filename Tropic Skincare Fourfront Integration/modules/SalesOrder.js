'use strict';

/**
 *
 * This file is responsible to parse Sales Order data and Create
 * Sales Orders in NetSuite
 *
 * Version    Date                    Author           Remarks
 * 1.0.0      2020-11-03              Yasir
 *
 */
import log from 'N/log';
import _ from '../lib/lodash';
import moment from '../lib/moment';

import NSCrud from '../lib/f3_nscrud_base';
import globalConstants from '../globalConstants';
import Item from './Item';
import LogHandler from './LogHandler';
import ErrorLog from './ErrorLog';
import QueueMaster from './QueueMaster';
import Customer from './Customer';
import JournalModule from './Journal';
import IncoTermEntity from './IncoTerm';

let salesOrderEntity;
let SEARCH_OPERATOR;
let syncFlow;

const {
  LAST_RECORD_SYNC,
  SUBLIST,
  SALES_ORDER,
  THRESHOLD_LIMIT,
  ITEM,
  COMMON,
  FF_INTEGRATION_MAPPING,
  CONFIG,
  CONFIG_PATH,
  INCO_TERM_LOOKUP,
} = globalConstants;

/**
 * Check Data Mismatch
 * @param {Object} refObj
 * @param {array} ItemIds
 * @param {Array} dataToImport
 * @returns {Boolean}
 */
const checkDataMismatch = (refObj, ItemIds = [], dataToImport = []) => {
  const commentTag = 'SalesOrder => checkDataMismatch';
  const isMismatch = (ItemIds.length !== dataToImport.length);

  if (isMismatch) {
    const csvData = _.compact(_.map(dataToImport, ITEM.SKU_CODE));
    // Here ternary operator is used because _.difference method of lodash
    // computes difference of first object with second.
    const differenceData = csvData.length > ItemIds.length
      ? _.difference(csvData, ItemIds) : _.difference(ItemIds, csvData);
    log.audit(`${commentTag} => Missing Items`, differenceData);

    // Throwing error for Data Mismatch and logging ending details of script.
    LogHandler.commit({ lastSyncTimeCurrent: true });
    ErrorLog.dataMismatch({
      messageDetails: 'Data has some mismatch in netsuite',
      recordType: COMMON.SALES_ORDER,
      recordReference: refObj.orderReference,
      filename: refObj.filename,
      syncFlow,
      data: differenceData,
      throwError: false,
    });
  }
  return isMismatch;
};

/**
 * Search Inco Term using IncoTerm Module
 * @param {String} country
 * @returns {String}
 */
const getIncoTerm = (country) => {
  if (country && country.trim()) {
    const searchParam = {
      filters: [
        [INCO_TERM_LOOKUP.COUNTRY_CODE, SEARCH_OPERATOR.IS, country],
      ],
      columns: [
        { name: COMMON.INTERNAL_ID },
        { name: INCO_TERM_LOOKUP.INCO_TERM },
      ],
    };
    const incoTermResult = IncoTermEntity.getIncoTerm(searchParam);
    return incoTermResult && incoTermResult.length > 0
      ? incoTermResult[0][INCO_TERM_LOOKUP.INCO_TERM] : COMMON.INCO_TERM_DDP;
  }
};

/**
 * Set Line Fields of Sales Order
 * @param {Object} salesRecord
 * @param {Object} lineObject
 */
const setLineItem = (salesRecord, lineObject) => {
  const sublistId = SUBLIST.ITEM;
  const inventorySublist = salesRecord.selectNewLine({ sublistId });

  // Setting each field from Line Object
  Object.keys(lineObject).forEach((fieldId) => {
    let fieldValue = lineObject[fieldId];

    // Replacing Y or N with true and false
    if (fieldValue === COMMON.CSV_TRUE || fieldValue === COMMON.CSV_FALSE
      || fieldId === SALES_ORDER.LINE_ITEMS.DIRECT_DELIVERY_INDICATOR) {
      fieldValue = fieldValue === COMMON.CSV_TRUE;
    }

    inventorySublist.setCurrentSublistValue({
      sublistId,
      fieldId,
      fieldValue,
    });
  });
  salesRecord.commitLine({
    sublistId,
  });
};

/**
 * Search Customer Internal Id using Customer Module
 * @param {String} entity
 * @returns {Array}
 */
const getCustomerInternalId = (entity) => Customer.getCustomer(entity);

/**
 * Search for Items in Netsuite
 * @param {Array} dataToImport
 * @param {Object} param1
 * @returns {Boolean}
 */
const verifyItemsInNetsuite = (dataToImport, { orderReference, filename }) => {
  const commentTag = 'SalesOrder => verifyItemsInNetsuite';
  // Extracting all Items SKU to get internal Id's in netsuite.
  const itemCodes = _.compact(_.map(dataToImport, COMMON.ITEM));
  log.debug(`${commentTag} => Item Codes External System`, itemCodes);

  // Searching Items in Netsuite
  const itemIds = Item.getItem(itemCodes);
  log.debug(`${commentTag} => Items in Netsuite`, itemIds);

  // Checking for any non existing Item in Netsuite.
  return checkDataMismatch({ orderReference, filename }, _.uniq(itemCodes), itemIds);
};

/**
 * Search for customer and create one if not found
 * @param {Object} customerData
 * @param {String} entity
 * @returns {String}
 */
const getCustomer = (customerData, entity) => {
  let customerId = getCustomerInternalId(entity);

  if (customerId && customerId.length === 0) {
    customerId = Customer.createCustomer(customerData);
    if (customerId && customerId.length > 0) {
      customerId = customerId[0].internalid;
    } else {
      // Throwing Customer Cannot be created Error
      const obj = { name: 'Error', message: 'Unable to create Sales Order due to customer creation failure' };
      throw obj;
    }
  }

  log.audit('SalesOrder => createSalesOrder', `Customer Internal Id Netsuite ${customerId}`);
  return customerId;
};

/**
 * Create Journal Entries Using Journal Module
 * @param {Array} dataToImport
 * @param {String} entity
 */
const createJournalEntries = (dataToImport, entity) => {
  const paymentLines = dataToImport.filter((obj) => obj.indicator === COMMON.PAYMENT_INDICATOR);

  // Create the Journal along with sales order
  paymentLines.forEach((paymentObj) => {
    const referenceValues = { customer: entity };
    JournalModule.createJournal(referenceValues, paymentObj);
  });
};

/**
 * Creates Sales order and Sales Journal Entries
 * @param {Object} bodyData
 * @param {Array} dataToImport
 * @param {Array} itemIds
 * @returns {String}
 */
const createSalesOrder = (bodyData, dataToImport, itemIds) => {
  const commentTag = 'SalesOrder => createSalesOrder';

  // Create Sales Order with body fields
  const salesObject = salesOrderEntity.create(bodyData, { isDynamic: true });

  // Extracting Sales Order Lines Data
  const salesOrderLines = dataToImport;
  salesOrderLines.shift();

  // Insert Line Items in Sales Order.
  salesOrderLines.forEach((inventory) => {
    const {
      item,
    } = inventory;

    const productCode = _.find(itemIds, { feedname: item });

    // Replace Item name with internalid
    const itemId = (productCode && productCode.internalid) || '';

    if (itemId) {
      _.assign(inventory, { item: itemId });
      if (!inventory.price) { _.assign(inventory, { price: '-1' }); }
      setLineItem(salesObject, inventory);
    }
  });

  const salesOrderId = salesOrderEntity.saveRecord(salesObject);
  log.audit(`${commentTag} => SO Created`, salesOrderId);

  createJournalEntries(dataToImport, bodyData.entity);

  return salesOrderId;
};

/**
 * Parsing Sales Order data
 * @param {Object} context
 * @returns {String}
 */
const prepareSalesOrderData = (context) => {
  const commentTag = 'SalesOrder => prepareSalesOrderData';
  const {
    orderReference,
    dataToImport,
    filename,
  } = context;

  try {
    if (verifyItemsInNetsuite(dataToImport, { orderReference, filename })) {
      return true;
    }
    log.audit(`${commentTag} => Sales Order Data`, dataToImport);

    // Removed the customer creation releated data
    const customerData = dataToImport.pop();

    const bodyData = _.head(dataToImport);
    log.debug(`${commentTag} => Sales Order Body Data`, bodyData);

    // Search customer in NetSuite and create new if not present
    const customerId = getCustomer(customerData, bodyData.entity);

    // Parsing Data Object to NetSuite Format
    const trandate = salesOrderEntity.parseToValue({
      value: (bodyData.trandate && moment(bodyData.trandate, COMMON.DATE_FORMAT).toDate())
      || moment().toDate(),
      type: 'datetime',
      timezone: COMMON.TIME_ZONE,
    });

    _.assign(bodyData, { entity: customerId, trandate, custbody_f3_so_filename: filename });

    // Populate Inco Term based on country on Sales Order
    const incoTermMap = getIncoTerm(bodyData.custbody_f3_add_country);
    bodyData[SALES_ORDER.INCOTERM] = incoTermMap;
    log.debug(`${commentTag} => Body Data`, bodyData);

    const salesOrderId = createSalesOrder(bodyData, dataToImport);

    if (salesOrderId) {
      LogHandler.recordSynched();
    }

    return salesOrderId;
  } catch (error) {
    LogHandler.recordFailed();
    ErrorLog.recordNotCreated({
      messageDetails: `Error: ${error.name} : ${error.message}`,
      recordType: COMMON.SALES_ORDER,
      syncFlow,
      throwError: false,
      recordReference: orderReference,
      filename,
    });
  }
};

/**
 * Extract Sales Order Data.
 * @param {Object} context
 * @returns {Array}
 */
const importSalesOrder = (context) => {
  const commentTag = 'SalesOrder => importSalesOrder';
  log.audit(`${commentTag} => Context`, context);

  salesOrderEntity.scriptStartTime();

  // Initializing Script log details. This is a custom record which logs all
  // the details like Script Start time, end time, Total Records Processed, etc.
  LogHandler.init({ actionType: syncFlow, syncFlow, syncType: COMMON.SALES_ORDER });

  const { data = [], filename = 'filename missing' } = context;
  const importedSOIds = [];
  const { dataToImport } = data;

  // Nested loop was required due to project specific data structure received
  for (let i = 0; i < dataToImport.length; i += 1) {
    for (let j = 0; j < dataToImport[i].length; j += 1) {
      const singleSoData = dataToImport[i][j];

      // Pusing Customer Data on last Index of Sales Order. Here data was provided
      // in format of single object in each array index.
      singleSoData.push(data.mappedCustomerData[i][0]);
      const orderReference = singleSoData[0].custbody_f3_es_tranid;

      // Creating Data Object for Sales Order creation
      const dataObject = { orderReference, dataToImport: singleSoData, filename };

      // Checking for time elapsed. In order to delegate tasks for later execustion
      // through map reduce.
      if (salesOrderEntity.scriptElapsedTime() > THRESHOLD_LIMIT.MAX_TIME) {
        // Pushing records in a Queue (Custom Record) to be processed later.
        if (QueueMaster.isEmpty()) {
          LogHandler.totalRecordsProcessed(i);

          QueueMaster.createRecord({
            filename,
            totalRecords: data.dataToImport.length - i,
          });
        }

        QueueMaster.enqueue({
          recordType: FF_INTEGRATION_MAPPING.SALES_ORDER,
          recordReference: i,
          data: dataObject,
        });
        continue;
      }
      importedSOIds.push(prepareSalesOrderData(dataObject));
    }
  }

  // Committing records in Custom Record.
  if (!QueueMaster.isEmpty()) {
    QueueMaster.commitQueueLines();
  }

  // Logging ending details of script.
  LogHandler.commit({
    lastSyncTimeCurrent: true,
    status: LAST_RECORD_SYNC.SUCCESS,
    actionType: syncFlow,
  });

  return importedSOIds;
};

/**
 * Initialize NSCrud and Environment Variables
 * @param {Object} BaseModule
 * @returns {Object}
 */
const init = (BaseModule) => {
  salesOrderEntity = BaseModule;

  salesOrderEntity.recordType = salesOrderEntity.getRecordType().SALES_ORDER;
  SEARCH_OPERATOR = salesOrderEntity.getSearchOperator();

  syncFlow = 'IMPORT';

  // eslint-disable-next-line no-proto
  CONFIG.__proto__ = salesOrderEntity.loadEnvironmentConfig(CONFIG_PATH);

  return {
    importSalesOrder,
    prepareSalesOrderData,
  };
};

export default init(new NSCrud.Entity());
