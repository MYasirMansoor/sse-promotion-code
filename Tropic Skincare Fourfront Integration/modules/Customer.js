'use strict';

/**
 *
 * This file is responsible to Search or Create Customer in NetSuite
 *
 * Version    Date                    Author           Remarks
 * 1.0.0      2020-11-03              Yasir
 *
 */

import log from 'N/log';
import _ from '../lib/lodash';

import NSCrud from '../lib/f3_nscrud_base';
import globalConstants from '../globalConstants';
import ErrorLog from './ErrorLog';

const {
  COMMON,
  CUSTOMER,
} = globalConstants;

let CustomerEntity;
let syncFlow;
let SEARCH_OPERATOR;

/**
 * Execute and extract saved search results using NSCrud
 * @param {Object} context
 * @returns {Array}
 */
const executeSearch = (context = {}) => {
  try {
    const customerSearch = CustomerEntity.search(context);
    const customerRecord = CustomerEntity.getAllRecords(customerSearch);
    const customerParsedRecord = CustomerEntity.parseSearchResults(customerRecord);
    return customerParsedRecord;
  } catch (error) {
    ErrorLog.searchNotExecuted({
      messageDetails: error.name, recordType: COMMON.CUSTOMER_TYPE, syncFlow,
    });
  }
};

/**
 * Create Search Columns and Filter
 * @param {String} entity
 * @returns {Array}
 */
const getCustomer = (entity = []) => {
  CustomerEntity.columns = [
    { name: COMMON.INTERNAL_ID, type: COMMON.ByValue },
  ];

  const filters = [
    [COMMON.IS_INACTIVE, SEARCH_OPERATOR.IS, COMMON.IS_FALSE],
    'AND',
    [CUSTOMER.ENTITY_ID, SEARCH_OPERATOR.IS, entity],
  ];

  return _.map(executeSearch({ filters }), (value) => ({
    [COMMON.INTERNAL_ID]: value[COMMON.INTERNAL_ID],
  }));
};

/**
 * Create Customer in NetSuite
 * @param {Object} context
 * @returns {Array}
 */
const createCustomer = (context) => {
  const commentTag = 'Customer => createCustomer';
  try {
    const { bodyObj, addObj } = context;

    // Creating Customer Object with Body Fields
    let customerObj = CustomerEntity.create(bodyObj, { isDynamic: true });
    log.debug(`${commentTag} => Customer Object`, JSON.stringify(customerObj));

    // Creating Customer Address
    customerObj = CustomerEntity.createCustomerAddress(customerObj, [{ address: addObj }]);

    const customerId = CustomerEntity.saveRecord(customerObj);
    return [{ internalid: customerId }];
  } catch (error) {
    ErrorLog.recordNotCreated({
      messageDetails: `Error: ${error.name} : ${error.message}`,
      recordType: COMMON.CUSTOMER_TYPE,
      syncFlow,
      throwError: false,
      recordReference: context,
    });
  }
};

/**
 * Initialize NSCrud Object
 * @param {Object} BaseModule
 */
const init = (BaseModule) => {
  CustomerEntity = BaseModule;
  CustomerEntity.recordType = CustomerEntity.getRecordType().CUSTOMER;
  syncFlow = 'IMPORT';
  SEARCH_OPERATOR = CustomerEntity.getSearchOperator();

  return {
    getCustomer,
    createCustomer,
  };
};

export default init(new NSCrud.Entity());
