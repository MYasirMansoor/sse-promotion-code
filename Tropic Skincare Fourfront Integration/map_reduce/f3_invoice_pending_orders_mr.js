'use strict';

/**
 *
 * This file is responsible to Invocie all Pending Billing Sales Orders
 * Here we receive a saved search containing all fulfilled and unfulfilled
 * lines. We have to filter out fulfilled lines and create an Invoice for
 * Sales Order
 *
 * Version    Date                    Author           Remarks
 * 1.0.0      2021-05-20              Yasir
 *
 */

/**
 * @NApiVersion 2.x
 * @NScriptType MapReduceScript
 * @NModuleScope Public
 */

import log from 'N/log';
import search from 'N/search';
import record from 'N/record';
import nsRuntime from 'N/runtime';

import globalConstants from '../globalConstants';
import ErrorLog from '../modules/ErrorLog';

const {
  SUBLIST,
  SALES_ORDER,
  COMMON,
  ITEM_FULFILLMENT,
  SCRIPT_PARAMS_PEND_ORDERS,
} = globalConstants;

/**
 * Create and run saved search to get all fulfillments of an Order
 * @param {String} salesOrderId
 * @returns {Array}
 */
const getAllFulfillments = (salesOrderId) => {
  try {
    if (salesOrderId) {
      const itemfulfillmentSearchObj = search.create({
        type: search.Type.ITEM_FULFILLMENT,
        filters:
        [
          [COMMON.TYPE, 'anyof', ITEM_FULFILLMENT.TYPE],
          'AND',
          [COMMON.MAIN_LINE, 'is', COMMON.IS_TRUE],
          'AND',
          [COMMON.CREATED_DATE, 'anyof', salesOrderId],
        ],
        columns:
        [
          search.createColumn({ name: COMMON.TRAN_ID }),
          search.createColumn({ name: COMMON.INTERNAL_ID }),
        ],
      });

      const pagedSearch = itemfulfillmentSearchObj.runPaged({ pageSize: 1000 });
      const pagedResults = pagedSearch.fetch({ index: 0 });
      return pagedResults.data;
    }
  } catch (error) {
    ErrorLog.searchNotExecuted({
      messageDetails: `Error: ${error.name} : ${error.message}`, recordType: COMMON.INVOICE_TYPE,
    });
  }
};

/**
 * Load and return search results
 * @param {String} searchId
 * @param {String} range
 * @returns {Object}
 */
const getSearchResults = (searchId, range) => {
  if (searchId) {
    const soSearch = search.load({
      id: searchId,
    });

    const resultsArray = [];
    if (range > 0) {
      // Extracting Saved search results. Here if range is greater than 1000
      // than its broken down into multiple results extraction and appended into single Array
      const pagedResults = soSearch.runPaged({ pageSize: 1000 });
      const iterations = parseInt(range) / 1000;
      for (let i = 0; i < iterations; i += 1) {
        // Extracting saved search results and appending into Array using spread operator
        resultsArray.push(...pagedResults.fetch({ index: i }).data);
      }
      return resultsArray;
    }
    return soSearch;
  }
  return [];
};

/**
 * Populate missing Unique Line ID on Item Fulfillment.
 * Unique Line Id is a unique Identifier in Fourfront
 * Sales orders for each line. We use this identifier
 * in creation of all records like Invoice, Sales Order, etc.
 * If this Identifier is missing in case of ammended order
 * from UI then further flow cannot execute.
 * @param {Object} fRecord
 */
const populateUniqueLineId = (fRecord) => {
  // Getting Line Number where Unique Line ID is missing
  let lineNumber = fRecord.findSublistLineWithValue({
    sublistId: SUBLIST.ITEM,
    fieldId: SALES_ORDER.LINE_ITEMS.UNIQUE_ID,
    value: '',
  });

  // Getting Linenumber of missing Unique Id and populating untill non left.
  while (lineNumber !== -1) {
    fRecord.selectLine({
      sublistId: SUBLIST.ITEM,
      line: lineNumber,
    });

    // Getting Orderline Number of Sales Order
    const orderLine = fRecord.getCurrentSublistValue({
      sublistId: SUBLIST.ITEM,
      fieldId: ITEM_FULFILLMENT.LINE_ITEMS.ORDER_LINE,
    });

    fRecord.setCurrentSublistValue({
      sublistId: SUBLIST.ITEM,
      fieldId: SALES_ORDER.LINE_ITEMS.UNIQUE_ID,
      value: orderLine,
    });

    fRecord.commitLine({
      sublistId: SUBLIST.ITEM,
    });

    lineNumber = fRecord.findSublistLineWithValue({
      sublistId: SUBLIST.ITEM,
      fieldId: SALES_ORDER.LINE_ITEMS.uniqueId,
      value: '',
    });
  }
};

/**
 * Get Script Params and execute search
 * @param {Object} context
 * @returns {Object}
 */
const getInputData = (context) => {
  const commentTag = 'f3_invoice_pending_orders_mr => getInputData';
  log.debug(`${commentTag} => Context`, context);

  // Getting Script Parameters to extract saved search Id and range of search results
  const currentScript = nsRuntime.getCurrentScript();
  const savedSearchId = currentScript.getParameter({ name: SCRIPT_PARAMS_PEND_ORDERS.SEARCH_ID }) || '';
  const range = currentScript.getParameter({ name: SCRIPT_PARAMS_PEND_ORDERS.RANGE }) || 0;

  return getSearchResults(savedSearchId, range);
};

/**
 * Merge Order lines under single key
 * @param {Object} context
 */
const map = (context) => {
  const commentTag = 'f3_invoice_pending_orders_mr => map';
  try {
    log.debug(`${commentTag} => context`, context);
    // Extracting results
    const results = JSON.parse(context.value);
    log.debug(`${commentTag} => Result`, results);

    // Grouping Results based on Internal Id. Here the data consist of multiple lines and
    // we had to perform operation over Sales Order Only.
    context.write(results[COMMON.INTERNAL_ID], results);
  } catch (error) {
    // Here Error log module is not used.
    // Reason behind is that this script was suppose to process a large
    // number of records having different kind of errors. So putting
    // Error log module here will create the error log in Custom Record
    // and eventually will be emailed to Client. This will possibly
    // hide any important error of already running integration.
    log.error(`${commentTag} => Error`, error);
  }
};

/**
 * Resubmit Item Fulfillments of an order to run UE for creation of Invoice
 * @param {Object} context
 */
const reduce = (context) => {
  // In reduce function NSCrud is not used to create or execute search
  // Reason behind is that the reduce function has a different context
  // for each instance which will force it create new Object everytime
  const commentTag = 'f3_invoice_pending_orders_mr => reduce';
  try {
    // Extracting Grouped data
    const { values } = context;
    log.debug(`${commentTag} => Grouped Data`, values);

    // Filtering for fulfilled line in order to bill the order. We needed only one
    // line which is fulfilled to completely bill order
    const isFulfilled = values.filter((exp) => JSON.parse(exp).shiprecvstatusline === 'Yes');
    log.debug(`${commentTag} => Fulfilled Lines`, isFulfilled);

    if (isFulfilled.length > 0) {
      // Extracting Sales Order Id
      const salesOrderId = JSON.parse(values[0])[COMMON.INTERNAL_ID] || '';
      log.audit(`${commentTag} => Sales order Id`, salesOrderId);

      // Search for All Fulfillments of Sales Order
      const fulfillmentRecords = getAllFulfillments(salesOrderId);
      // Resubmitting Fulfillments so that Userevent for Invoice Creation execute
      fulfillmentRecords.forEach((rec) => {
        const fRecord = record.load({
          type: rec.recordType,
          id: rec.id,
          isDynamic: true,
        });

        // Populate missing Unique Line Id's on Item Fulfillment
        populateUniqueLineId(fRecord);
        fRecord.save();
      });
    }
  } catch (error) {
    // Here Error log module is not used.
    // Reason behind is that this script was suppose to process a large
    // number of records having different kind of errors. So putting
    // Error log module here will create the error log in Custom Record
    // and eventually will be emailed to Client. This will possibly
    // hide any important error of already running integration.
    log.error(`${commentTag} => Error`, error);
  }
};

/**
 * Print Script Summary
 * @param {Object} summary
 */
const summarize = (summary) => {
  // Log execution details
  const type = summary.toString();
  log.audit(`${type} Usage Consumed`, summary.usage);
  log.audit(`${type} Number of Queues`, summary.concurrency);
  log.audit(`${type} Number of Yields`, summary.yields);
};

export default {
  getInputData,
  map,
  reduce,
  summarize,
};
