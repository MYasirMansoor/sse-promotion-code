'use strict';

/**
 *
 * This file is responsible to stop duplicate entries in Shipping Method
 * Custom Record in NetSuite
 *
 * Version    Date                    Author           Remarks
 * 1.0.0      2020-11-03              Yasir
 *
 */

/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 */

import log from 'N/log';
import error from 'N/error';

import NSCrud from '../lib/f3_nscrud_base';
import globalConstants from '../globalConstants';
import ErrorLog from './ErrorLog';

let shippingMethodEntity;
let SEARCH_OPERATOR;

const {
  COMMON,
  SHIPPING_METHOD,
} = globalConstants;

/**
 * Throw error using Error module
 */
const throwError = () => {
  throw error.create({
    name: 'Duplicate Record',
    message: 'Record with this country already exist',
    notifyOff: false,
  });
};

/**
 * Create and Execute Search
 * @param {Object} context
 * @returns {Array}
 */
const executeSearch = (context) => {
  try {
    const shippingMethodSearch = shippingMethodEntity.search(context);
    const shippingMethodRecord = shippingMethodEntity.getAllRecords(shippingMethodSearch);
    const shippingMethodParsedRecord = shippingMethodEntity
      .parseSearchResults(shippingMethodRecord);
    return shippingMethodParsedRecord;
  } catch (e) {
    ErrorLog.searchNotExecuted({
      messageDetails: e.name, recordType: SHIPPING_METHOD.TYPE,
    });
  }
};

/**
 * Create a search to check for duplicate shipping method
 * @param {Object} context
 * @returns {Array}
 */
const checkDuplicates = (context) => {
  const commentTag = 'f3_shipping_method_duplicate_handing_ue => checkDuplicates';
  const { newRecord } = context;

  // Getting Country Id
  const country = newRecord.getValue({
    fieldId: SHIPPING_METHOD.COUNTRY,
  });

  // Creating Columns for Search
  shippingMethodEntity.columns = [
    { name: SHIPPING_METHOD.COUNTRY, type: COMMON.ByValue },
  ];

  // Filtering on Country
  const filters = [
    [SHIPPING_METHOD.COUNTRY, SEARCH_OPERATOR.IS, country],
  ];

  const searchResults = executeSearch({ filters });
  log.debug(`${commentTag} => Results`, searchResults);
  return searchResults;
};

/**
 * Initialize Nscrud Object
 */
const initializaNsCrudObject = () => {
  shippingMethodEntity = new NSCrud.Entity();

  shippingMethodEntity.recordType = SHIPPING_METHOD.TYPE;
  SEARCH_OPERATOR = shippingMethodEntity.getSearchOperator();
};

/**
 * Run on submission of shipping method in a Custom Record
 * and manages that the method is not duplicated by client
 * @param {Object} context
 */
const beforeSubmit = (context) => {
  const commentTag = 'f3_shipping_method_duplicate_handing_ue => beforeSubmit';
  if (context.type !== COMMON.CONTEXT_CREATE) {
    return;
  }

  initializaNsCrudObject();

  log.audit(`${commentTag} => context`, context);

  const duplicateResults = checkDuplicates(context);

  if (duplicateResults && duplicateResults.length > 0) {
    throwError();
  }
};

export default {
  beforeSubmit,
};
