'use strict';

/**
 *
 * This file is responsible to Populate Unique Line ID (unqiue Identifier of Fourfront Integration)
 * when order is created/edited from UI or CSV Import
 *
 * Version    Date                    Author           Remarks
 * 1.0.0      2021-02-04              Yasir
 *
 */

/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 */

import log from 'N/log';
import runtime from 'N/runtime';

import globalConstants from '../globalConstants';
import ErrorLog from '../modules/ErrorLog';

const {
  SUBLIST,
  SALES_ORDER,
  COMMON,
} = globalConstants;

/**
 * Set Orderline number as Unique Line Id
 * @param {*} lineNumber
 * @param {*} salesRecord
 * @param {*} uniqueId
 * @returns
 */
const populateUniqueLineId = (lineNumber, salesRecord, uniqueId) => salesRecord.setSublistValue({
  sublistId: SUBLIST.ITEM,
  fieldId: SALES_ORDER.LINE_ITEMS.UNIQUE_ID,
  value: uniqueId,
  line: lineNumber,
});

/**
 * Calculate and set Prorated VAT for Invoicing Sales order
 * prorated vat = vat/quantity
 * @param {String} lineNumber
 * @param {Object} salesRecord
 */
const populateProrateVat = (lineNumber, salesRecord) => {
  // Extracting VAT and Quantity to calculate Prorated VAT
  const vat = salesRecord.getSublistValue({
    sublistId: SUBLIST.ITEM,
    fieldId: SALES_ORDER.LINE_ITEMS.VAT,
    line: lineNumber,
  });

  const qty = salesRecord.getSublistValue({
    sublistId: SUBLIST.ITEM,
    fieldId: SALES_ORDER.LINE_ITEMS.QUANTITY,
    line: lineNumber,
  });

  salesRecord.setSublistValue({
    sublistId: SUBLIST.ITEM,
    fieldId: SALES_ORDER.LINE_ITEMS.PRORATED_VAT,
    value: (parseFloat(vat) / parseInt(qty)).toFixed(9),
    line: lineNumber,
  });
};

/**
 * Loop over an Order Items and check any missing unique line id
 * @param {Object} context
 */
const checkMissingUniqueLineIds = (context) => {
  const commentTag = 'f3_populate_cust_fields_ue => checkMissingUniqueLineIds';
  const { newRecord } = context;

  const lines = newRecord.getLineCount({
    sublistId: SUBLIST.ITEM,
  });

  // Looping on each line to check for missing Unique ID
  for (let i = 0; i < lines; i += 1) {
    const uniqueLineId = newRecord.getSublistValue({
      sublistId: SUBLIST.ITEM,
      fieldId: SALES_ORDER.LINE_ITEMS.UNIQUE_ID,
      line: i,
    });

    const orderLine = newRecord.getSublistValue({
      sublistId: SUBLIST.ITEM,
      fieldId: SALES_ORDER.LINE_ITEMS.LINE,
      line: i,
    });

    // If Unique Id is missing populate it with order line number
    if (!uniqueLineId) {
      populateUniqueLineId(i, newRecord, orderLine);
      log.audit(`${commentTag} => Unique Id Populated`, orderLine);

      // Calculate and populate Prorated VAT
      // This runs only when Unique Id is missing because only
      // UI/CSV Import lines/orders are missing this identifier
      // and prorated vat
      populateProrateVat(i, newRecord);
      log.audit(`${commentTag} => Prorated VAT Populated`, orderLine);
    }
  }
};

/**
 * Populate Unique Line ID and Prorated VAT in case
 * order is created or edited from UI and CSV Import
 * @param {Object} context
 */
const beforeSubmit = (context) => {
  const commentTag = 'f3_populate_cust_fields_ue => beforeSubmit';

  try {
  // Checking context to run script only on Create or Edit Mode
  // Checking execution context to run script only when record is
  // created or edited using User Interface or CSV Import
    if ((context.type !== COMMON.CONTEXT_CREATE || context.type !== COMMON.CONTEXT_EDIT)
    && (runtime.executionContext !== runtime.ContextType.USER_INTERFACE
      || runtime.executionContext !== runtime.ContextType.CSV_IMPORT)) {
      return;
    }
    checkMissingUniqueLineIds(context);
    log.debug(`${commentTag} => before Submit`, 'Populate Unique Ids');
  } catch (error) {
    ErrorLog.recordNotCreated({
      messageDetails: `Error: ${error.name} : ${error.message}`,
      recordType: COMMON.SALES_ORDER,
      throwError: false,
    });
  }
};

export default {
  beforeSubmit,
};
