'use strict';

/**
 *
 * This file is responsible to create a Resend Button on Sales Order
 * On click of This Button a copy of same order with 0 total should be created
 *
 * Version    Date                    Author           Remarks
 * 1.0.0      2021-02-04              Yasir
 *
 */

/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 */

import log from 'N/log';

import globalConstants from '../globalConstants';

const {
  COMMON,
} = globalConstants;

const buttonEnum = {
  BUTTON_ID: 'custpage_resend_button',
  BUTTON_LABEL: 'Resend',
  CALL_BACK_FUNCTION: 'resendOrder',
  CLIENT_SCRIPT_PATH: '/SuiteScripts/Folio3/netsuite_fourfront/client_script/copy_order_zero_cs.js',
};

/**
 * Create Button on Form and attach Client Script
 * @param {Object} context
 */
const createResendButton = (context) => {
  const { form } = context;
  form.addButton({
    id: buttonEnum.BUTTON_ID,
    label: buttonEnum.BUTTON_LABEL,
    functionName: buttonEnum.CALL_BACK_FUNCTION,
  });

  form.clientScriptModulePath = buttonEnum.CLIENT_SCRIPT_PATH;
};

/**
 * This function is responsible to create a button on Sales order record
 * @param {Object} context
 */
const beforeLoad = (context) => {
  const commentTag = 'f3_create_zero_order_ue => beforeLoad';
  try {
    if (context.type !== COMMON.CONTEXT_VIEW) {
      return;
    }
    createResendButton(context);
  } catch (error) {
    log.error(`${commentTag} => beforeLoad`, error);
  }
};

export default {
  beforeLoad,
};
