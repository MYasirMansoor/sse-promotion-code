'use strict';

/**
 *
 * This file is responsible to populate all the accounts on Item
 * Record based on class by looking up from a Custom Record
 *
 * Version    Date                    Author           Remarks
 * 1.0.0      2021-11-15              Yasir
 *
 */

/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 */

import log from 'N/log';
import runtime from 'N/runtime';

import NSCrud from '../lib/f3_nscrud_base';
import globalConstants from '../globalConstants';
import ErrorLog from '../modules/ErrorLog';

const {
  CUSTOM_RECORD_TYPE,
  COMMON,
  ITEM_ACC_MAP,
  ITEM,
  ITEM_ACC_LOOKUP,
} = globalConstants;

let itemEntity;

/**
 * Execute and Return Search Results
 * @param {Object} context
 * @returns {Array}
 */
const executeSearch = (context) => {
  try {
    const itemSearch = itemEntity.search(context);
    const itemRecord = itemEntity.getAllRecords(itemSearch);
    const itemParsedRecord = itemEntity.parseSearchResults(itemRecord);
    return itemParsedRecord;
  } catch (error) {
    ErrorLog.searchNotExecuted({
      messageDetails: `Error: ${error.name} : ${error.message}`, recordType: COMMON.ITEM_TYPE,
    });
  }
};

/**
 * Create Filters and Columns for Accounts Search
 * @param {String} classId
 * @returns {Array}
 */
const getAccounts = (classId) => {
  // Creating Filter for Class
  const filters = [
    [ITEM_ACC_LOOKUP.CLASS, 'anyof', classId],
  ];

  // Pushing Columns in NSCrud Object
  const columns = [];
  for (const key in ITEM_ACC_LOOKUP) {
    columns.push({ name: ITEM_ACC_LOOKUP[key] });
  }
  itemEntity.columns = columns;

  return executeSearch({ filters });
};

/**
 * Initialize NSCrud Object
 */
const initializeNsCrudObject = () => {
  itemEntity = new NSCrud.Entity();
  itemEntity.recordType = CUSTOM_RECORD_TYPE.ITEM_ACC_LOOKUP;
};

/**
 * Extract Class and Populate Account on User Event
 * @param {Object} context
 */
const populateAccounts = (context) => {
  const commentTag = 'f3_populate_item_accounts_ue => populateAccounts';
  const { newRecord } = context;

  // Extracting class to lookup accounts
  const classId = newRecord.getValue({
    fieldId: ITEM.CATEGORY_ID,
  });

  const accounts = getAccounts(classId);
  log.debug(`${commentTag} => Accounts`, accounts);

  if (accounts && accounts.length > 0) {
    // Extracting 0th index here as there will be only 1 match for each class
    const account = accounts[0];

    // Looping over each field of accounts map and setting value from saved
    // search results
    for (const key in ITEM_ACC_MAP) {
      newRecord.setValue({
        fieldId: [ITEM_ACC_MAP[key]],
        value: account[ITEM_ACC_LOOKUP[key]],
      });
    }
  }
};

/**
 * @param {Object} context
 */
const beforeSubmit = (context) => {
  const commentTag = 'f3_populate_item_accounts_ue => beforeSubmit';
  try {
    // Checking context to run script only on Create Mode
    // Checking execution context to run script only when record is
    // created using CSV Import
    if (context.type !== COMMON.CONTEXT_CREATE
      && runtime.executionContext !== runtime.ContextType.CSV_IMPORT) {
      return;
    }
    // Initializing NSCrud Object
    log.debug(`${commentTag} => Before Submit`, 'Populating Accounts');
    initializeNsCrudObject();
    populateAccounts(context);
  } catch (error) {
    ErrorLog.recordNotCreated({
      messageDetails: `Error: ${error.name} : ${error.message}`,
      recordType: COMMON.ITEM_TYPE,
      throwError: false,
    });
  }
};

export default {
  beforeSubmit,
};
