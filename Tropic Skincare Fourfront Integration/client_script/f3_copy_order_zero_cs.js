'use strict';

/**
 *
 * This file is responsible to create a copy of Sales Order with 0 total
 *
 * Version    Date                    Author           Remarks
 * 1.0.0      2021-02-04              Yasir
 *
 */

/**
*@NApiVersion 2.x
*@NScriptType ClientScript
*/

import log from 'N/log';
import record from 'N/record';
import currentRecord from 'N/currentRecord';
import dialog from 'N/ui/dialog';
import url from 'N/url';

import globalConstants from '../globalConstants';
import ErrorLog from '../module/ErrorLog';

const {
  SUBLIST,
  SALES_ORDER,
  COMMON,
} = globalConstants;

/**
 * Page Initialization
 * @param {Object} context
 */
const pageInit = (context) => {
  const commentTag = 'f3_copy_order_zero_cs => pageInit';
  log.debug(`${commentTag} => pageInit`, context);
};

/**
 * Set all the order lines with 0 value
 * @param {Object} copiedRecord
 */
const setZeroLines = (copiedRecord) => {
  // Getting count of lines
  const lines = copiedRecord.getLineCount({
    sublistId: SUBLIST.ITEM,
  });

  // Looping on each line to set Rate, VAT and Prorate Vat as zero
  for (let i = 0; i < lines; i += 1) {
    copiedRecord.selectLine({
      sublistId: SUBLIST.ITEM,
      line: i,
    });

    copiedRecord.setCurrentSublistValue({
      sublistId: SUBLIST.ITEM,
      fieldId: SALES_ORDER.LINE_ITEMS.RATE,
      value: 0,
    });

    copiedRecord.setCurrentSublistValue({
      sublistId: SUBLIST.ITEM,
      fieldId: SALES_ORDER.LINE_ITEMS.VAT,
      value: 0,
    });

    copiedRecord.setCurrentSublistValue({
      sublistId: SUBLIST.ITEM,
      fieldId: SALES_ORDER.LINE_ITEMS.PRORATED_VAT,
      value: 0,
    });

    copiedRecord.commitLine({
      sublistId: SUBLIST.ITEM,
    });
  }
};

/**
 * Redirect to Copied Sales Order
 * @param {String} copiedSalesOrderId
 */
const redirectToCopiedOrder = (copiedSalesOrderId) => {
  if (!copiedSalesOrderId) { return; }

  // Displaying alert for Copied Order
  dialog.alert({
    title: 'Record Copied',
    message: 'Record is copied Please click Ok',
  }).then(() => {
    // Redirecting to copied order
    const output = url.resolveRecord({
      recordType: record.Type.SALES_ORDER,
      recordId: copiedSalesOrderId,
      isEditMode: false,
    });
    window.location.replace(output);
  });
};

/**
 * Creates a zero order
 */
const resendOrder = () => {
  const commentTag = 'f3_copy_order_zero_cs => resendOrder';
  try {
    // Getting Current Record
    const cRecord = currentRecord.get();
    const salesOrderId = cRecord.id;

    // Creating a copy of current order
    const copiedRecord = record.copy({
      type: record.Type.SALES_ORDER,
      id: salesOrderId,
      isDynamic: true,
    });

    // Setting all lines as zero
    setZeroLines(copiedRecord);

    const copiedSalesOrderId = copiedRecord.save();
    log.audit(`${commentTag} => Copied Order Id`, copiedSalesOrderId);

    redirectToCopiedOrder(copiedSalesOrderId);
  } catch (error) {
    ErrorLog.recordNotCreated({
      messageDetails: `Error: ${error.name} : ${error.message}`,
      recordType: COMMON.SALES_ORDER,
      throwError: false,
    });
  }
};

export default {
  pageInit,
  resendOrder,
};
