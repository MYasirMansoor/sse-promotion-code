'use strict';

/**
 *
 * This file is responsible to Create Inventory Adjustment Record in NetSuite
 * and Export Inventory Adjustments to Middleware
 *
 * Version    Date                    Author           Remarks
 * 1.0.0      2019-12-30              Yasir
 *
 */

import log from 'N/log';
import _ from '../lib/lodash';

import LogHandler from './LogHandler';
import RecordsFieldMapping from './RecordsFieldMapping';
import LastRecordSync from './LastRecordSync';
import ErrorLog from './ErrorLog';
import NSCrud from '../lib/f3_nscrud_base';
import globalConstants from '../globalConstants';

let inventoryAdjustmentEntity;
let syncFlow;
let syncType;
let SEARCH_OPERATOR;

const {
  LAST_RECORD_SYNC,
  SYNC_TYPE,
  INVENTORY_ADJUSTMENT,
  FF_INTEGRATION_MAPPING,
  CONFIG,
  CONFIG_PATH,
  COMMON,
  SUBLIST,
  WORK_ORDER_ISSUE,
} = globalConstants;

const dateFormat = 'DD.MM.YYYY HH12:MI:SS AM';

const DEFAULT_COLUMNS = [
  {
    name: 'formulatext',
    formula: `TO_CHAR({${INVENTORY_ADJUSTMENT.DATE_UPDATED}}, '${dateFormat}')`,
    summary: COMMON.SUMMARY_GROUP,
  },
  { name: COMMON.INTERNAL_ID, type: COMMON.ByValue, summary: COMMON.SUMMARY_GROUP },
];

const searchParams = {
  columns: undefined,
  filters: undefined,
};

/**
 * Set inventory detail on Adjustment Record
 * @param {String} sublist
 * @param {Object} inventoryDetailSubrecord
 * @param {Object} data
 */
const setInventoryDetail = (sublist, inventoryDetailSubrecord, data = {}) => {
  // Selecting New Line in Inventory Detail Subrecord
  inventoryDetailSubrecord.selectNewLine({
    sublistId: sublist,
  });

  // Setting all line values
  for (const key in data) {
    inventoryDetailSubrecord.setCurrentSublistValue({
      sublistId: sublist,
      fieldId: key,
      value: data[key],
    });
  }

  inventoryDetailSubrecord.commitLine({
    sublistId: sublist,
  });
};

/**
 * Execute and extract saved search results
 * @param {Object} context
 * @returns {Array}
 */
const executeSearch = (context = {}) => {
  try {
    const adjustmentSearch = inventoryAdjustmentEntity.search(context);
    const adjustmentRecords = inventoryAdjustmentEntity.getAllRecords(adjustmentSearch);
    const adjustmentParsedRecords = inventoryAdjustmentEntity.parseSearchResults(adjustmentRecords);
    return adjustmentParsedRecords;
  } catch (error) {
    // Logging script ending details
    LogHandler.commit();
    LastRecordSync.updateSyncTime({
      recordType: syncType,
    });

    // Throwing Error in Custom record to be emailed
    ErrorLog.searchNotExecuted({
      messageDetails: error.name, recordType: syncType, syncFlow,
    });
  }
};

/**
 * Set Line Details of Inventory Adjustment
 * @param {Object} dataToImport
 * @param {String} quantity
 * @param {Object} adjustmentRecord
 */
const setAdjustmentLineValues = (dataToImport, quantity, adjustmentRecord) => {
  // Creating Lines Object to set values
  const lineValues = {
    [WORK_ORDER_ISSUE.ITEM]: dataToImport.item,
    [WORK_ORDER_ISSUE.LOCATION]: CONFIG.__proto__.LOCATIONS.MARCO_LOCATION,
    [WORK_ORDER_ISSUE.ADJUST_QUANTITY]: quantity,
  };

  // Selecting a new line. Here we does not loop because there will be always one item
  adjustmentRecord.selectNewLine({
    sublistId: SUBLIST.INVENTORY,
  });

  // Setting all line values
  _.forEach(lineValues, (value, fieldId) => {
    adjustmentRecord.setCurrentSublistValue({
      sublistId: SUBLIST.INVENTORY,
      fieldId,
      value,
    });
  });

  // Getting Inventory Detail Subrecord
  const inventoryDetailSubrecord = adjustmentRecord.getCurrentSublistSubrecord({
    sublistId: SUBLIST.INVENTORY,
    fieldId: WORK_ORDER_ISSUE.INVENTORY_DETAIL.ADJUSTMENT_INVENTORY_DETAIL,
  });

  // Setting Inventory Details on Adjustment
  setInventoryDetail(SUBLIST.INVENTORY_ASSIGNMENT, inventoryDetailSubrecord, {
    [WORK_ORDER_ISSUE.INVENTORY_DETAIL.ADJUSTMENT_INVENTORY_NUMBER]: dataToImport.lotnumber,
    [WORK_ORDER_ISSUE.QUANTITY]: quantity,
  });

  adjustmentRecord.commitLine({
    sublistId: SUBLIST.INVENTORY,
  });
};

/**
 * Create Inventory Adjustment in specified Lot Number
 * @param {String} quantity
 * @param {Object} dataToImport
 */
const createAdjustment = (quantity, dataToImport) => {
  const commentTag = 'InventoryAdjustment => createAdjustment';
  try {
    // Creating Adjustment Record
    const adjustmentRecord = inventoryAdjustmentEntity.create({
      account: CONFIG.__proto__.ACCOUNTS.ADJUSTMENT_ACCOUNT,
      [WORK_ORDER_ISSUE.ADJUSTMENT_REASON]: WORK_ORDER_ISSUE.ADJUSTMENT_REASON_OVERLOT,
    }, { isDynamic: true });

    // Setting Adjustment Line Details
    setAdjustmentLineValues(dataToImport, quantity, adjustmentRecord);

    const adjustmentId = inventoryAdjustmentEntity.saveRecord(adjustmentRecord);
    log.audit(`${commentTag} => Inventory Adjustment Created`, adjustmentId);
  } catch (error) {
    ErrorLog.recordNotCreated({
      messageDetails: `Error: ${error.name} : ${error.message}`,
      recordType: COMMON.INVENTORY_ADJUSTMENT,
      throwError: true,
    });
  }
};

/**
 * Create and append date filter in saved search
 * @param {String} lastSyncTime
 */
const applyUpdateDateFilter = (lastSyncTime) => {
  if (lastSyncTime) {
    const formulaExp = `formulanumeric: ADD_MONTHS({${INVENTORY_ADJUSTMENT.DATE_UPDATED}}, 0)-ADD_MONTHS(TO_DATE('${lastSyncTime}', '${dateFormat}'), 0)`;
    const updatedDateFilter = [
      'AND',
      [formulaExp, SEARCH_OPERATOR.GREATERTHAN, '0'],
    ];

    searchParams.filters.push(...updatedDateFilter);
  }
};

/**
 * Return last sync time from custom record in NS
 * @param {String} recordType
 * @returns {String}
 */
const getLastSyncTime = (recordType) => {
  const { syncTime } = LastRecordSync.getSyncTime({ recordType });
  return syncTime;
};

/**
 * Delete Defined property from object
 * @param {Array} mappedRecord
 * @param {String} propertyToDelete
 * @returns
 */
const deleteProperty = (mappedRecord, propertyToDelete) => _.map(
  _.values(mappedRecord),
  (mappedRecordObj) => _.map(mappedRecordObj, (obj) => delete obj[propertyToDelete]),
);

/**
 * Map Saved search results according to format reuqired in middleware
 * @param {Array} fieldsMapping
 * @param {Array} woParsedRecords
 * @returns {Array}
 */
const getMappedRecord = (fieldsMapping, woParsedRecords) => {
  const commentTag = 'InventoryAdjustment => getMappedRecord';
  // Mapping Searched Fields according to External System
  let mappedRecord = RecordsFieldMapping.mapRemaingFields(
    woParsedRecords,
    fieldsMapping,
    { recordType: syncType, syncFlow },
  );
  log.audit(`${commentTag} => Mapped Record`, mappedRecord);

  // Getting Index of internal id and line id for grouping and deletion
  const internalIdIndex = _.find(
    fieldsMapping,
    { nsField: INVENTORY_ADJUSTMENT.INTERNAL_ID },
  ).ffField;
  const lineIdIndex = _.find(
    fieldsMapping,
    { nsField: INVENTORY_ADJUSTMENT.LINE_ID },
  ).ffField;

  // grouping records by Internal Id
  mappedRecord = _.groupBy(mappedRecord, internalIdIndex);

  // Deleting Internal Id and Line Id as it is not required in CSV
  mappedRecord = deleteProperty(mappedRecord, internalIdIndex);
  mappedRecord = deleteProperty(mappedRecord, lineIdIndex);

  return mappedRecord;
};

/**
 * Return Common filters for Lot and NonLot Adjustments Search
 * @returns {Array}
 */
const itemsFilter = () => [
  [INVENTORY_ADJUSTMENT.TYPE, SEARCH_OPERATOR.ANYOF, INVENTORY_ADJUSTMENT.ADJUSTMENT_TYPE],
  'AND',
  [[
    [INVENTORY_ADJUSTMENT.LOCATION, SEARCH_OPERATOR.ANYOF,
      CONFIG.__proto__.LOCATIONS.MARCO_LOCATION_LONDON_HQ,
      CONFIG.__proto__.LOCATIONS.MARCO_LOCATION_LONDON_HQ_QUARANTINE,
      CONFIG.__proto__.LOCATIONS.MARCO_LOCATION_LONDON_HQ_QC_FAIL],
    'AND',
    [INVENTORY_ADJUSTMENT.MARCO_INGREDIENT, SEARCH_OPERATOR.IS, COMMON.IS_TRUE]],
  'OR',
  [[INVENTORY_ADJUSTMENT.LOCATION, SEARCH_OPERATOR.ANYOF,
    CONFIG.__proto__.LOCATIONS.MARCO_LOCATION_LONDON_HQ],
  'AND',
  [INVENTORY_ADJUSTMENT.MARCO_INGREDIENT, SEARCH_OPERATOR.IS, COMMON.IS_FALSE]]],
  'AND',
  [COMMON.MAIN_LINE, SEARCH_OPERATOR.IS, COMMON.IS_FALSE],
  'AND',
  [COMMON.TAX_LINE, SEARCH_OPERATOR.IS, COMMON.IS_FALSE],
  'AND',
  [COMMON.COGS_LINE, SEARCH_OPERATOR.IS, COMMON.IS_FALSE],
  'AND',
  [COMMON.SHIPPING_LINE, SEARCH_OPERATOR.IS, COMMON.IS_FALSE],
];

/**
 * Create Columns and Filters for search
 * @param {Array} fieldsMapping
 */
const createColumnsAndFiltersLotNumbered = (fieldsMapping) => {
  // Extracting columns from Fields Mapping
  const COLUMNS = RecordsFieldMapping.netSuiteSearchColumnFromMapping(fieldsMapping);
  COLUMNS.push({ name: INVENTORY_ADJUSTMENT.DATE_UPDATED, sort: COMMON.SORT.ASC, summary: 'GROUP' });

  // Appending fields mapping columns and default ones
  inventoryAdjustmentEntity.columns = [...COLUMNS, ...DEFAULT_COLUMNS];

  searchParams.filters = itemsFilter();

  const lotNumberedFilter = [
    [INVENTORY_ADJUSTMENT.IS_LOT_ITEM, SEARCH_OPERATOR.IS, COMMON.IS_TRUE],
    'AND',
    [INVENTORY_ADJUSTMENT.LOT_LOCATION, SEARCH_OPERATOR.ANYOF,
      CONFIG.__proto__.LOCATIONS.MARCO_LOCATION_LONDON_HQ,
      CONFIG.__proto__.LOCATIONS.MARCO_LOCATION_LONDON_HQ_QUARANTINE,
      CONFIG.__proto__.LOCATIONS.MARCO_LOCATION_LONDON_HQ_QC_FAIL],
  ];

  searchParams.filters.push('AND');
  searchParams.filters.push(...lotNumberedFilter);
};

/**
 * Create Columns and Filters for search
 * @param {Array} fieldsMapping
 */
const createColumnsAndFiltersNonLotNumbered = (fieldsMapping) => {
  // Extracting columns from Fields Mapping
  const COLUMNS = RecordsFieldMapping.netSuiteSearchColumnFromMapping(fieldsMapping);
  COLUMNS.push({ name: INVENTORY_ADJUSTMENT.DATE_UPDATED, sort: COMMON.SORT.ASC, summary: 'GROUP' });

  // Appending fields mapping columns and default ones
  inventoryAdjustmentEntity.columns = [...COLUMNS, ...DEFAULT_COLUMNS];

  searchParams.filters = itemsFilter();

  const nonLotNumberedFilter = [
    [INVENTORY_ADJUSTMENT.IS_LOT_ITEM, SEARCH_OPERATOR.IS, COMMON.IS_FALSE],
    'AND',
    [INVENTORY_ADJUSTMENT.INVENTORY_LOCATION, SEARCH_OPERATOR.ANYOF,
      CONFIG.__proto__.LOCATIONS.MARCO_LOCATION_LONDON_HQ,
      CONFIG.__proto__.LOCATIONS.MARCO_LOCATION_LONDON_HQ_QUARANTINE,
      CONFIG.__proto__.LOCATIONS.MARCO_LOCATION_LONDON_HQ_QC_FAIL],
  ];

  searchParams.filters.push('AND');
  searchParams.filters.push(...nonLotNumberedFilter);
};

/**
 * Create Columns and Filters for lot numbered IA search
 * @param {Array} fieldsMapping
 * @returns {Array}
 */
const createLotNumberedSaveSearch = (fieldsMapping) => {
  // Create Columns and Filters for item receipts search
  createColumnsAndFiltersLotNumbered(fieldsMapping);

  // Apply date filter from Custom Record "Last Rec Sync"
  // This function get's Last sync time from a custom record and create a
  // filter so that the records from last export cycle only comes in saved search.
  applyUpdateDateFilter(getLastSyncTime(syncType));

  return executeSearch(searchParams);
};

/**
 * Create Columns and Filters for non lot numbered IA search
 * @param {Array} fieldsMapping
 * @returns {Array}
 */
const createNonLotNumberedSaveSearch = (fieldsMapping) => {
  // Create Columns and Filters for item receipts search
  createColumnsAndFiltersNonLotNumbered(fieldsMapping);

  // searchParams.type = inventoryAdjustmentEntity.getSearchType().INVENTORY_ADJUSTMENT;

  // Apply date filter from Custom Record "Last Rec Sync"
  // This function get's Last sync time from a custom record and create a
  // filter so that the records from last export cycle only comes in saved search.
  applyUpdateDateFilter(getLastSyncTime(syncType));

  return executeSearch(searchParams);
};

/**
 * Create Search for Inventory Adjustments
 * @param {Array} fieldsMappingLotNumbered
 * @param {Array} fieldsMappingNonLotNumbered
 * @returns {Array}
 */
const getInventoryAdjustments = (fieldsMappingLotNumbered, fieldsMappingNonLotNumbered) => {
  const commentTag = 'InventoryAdjustment => getInventoryAdjustments';

  // Create seperate saved search for lot and non lot items
  /**
   * Here we have create different searches for Lot Numbered and Non Lot Numbered Items
   * due to reason that the filters were a bit complex and stacking both in single
   * saved search were not producing correct results
   */
  const adjustmentParsedRecordsLotNumbered = createLotNumberedSaveSearch(
    fieldsMappingLotNumbered,
  );
  const adjustmentParsedRecordsNonLotNumbered = createNonLotNumberedSaveSearch(
    fieldsMappingNonLotNumbered,
  );

  // Extract latest last execution time (field name in search was formulatext) from lot and non lot
  let lastExTime = adjustmentParsedRecordsLotNumbered
  && adjustmentParsedRecordsLotNumbered.length > 0 ? _.last(adjustmentParsedRecordsLotNumbered).formulatext : '';
  const lastExTimeNonLot = adjustmentParsedRecordsNonLotNumbered
  && adjustmentParsedRecordsNonLotNumbered.length > 0 ? _.last(adjustmentParsedRecordsNonLotNumbered).formulatext : '';

  lastExTime = lastExTime > lastExTimeNonLot ? lastExTime : lastExTimeNonLot;

  // Combine saved search results
  const adjustmentParsedRecords = adjustmentParsedRecordsLotNumbered
    .concat(adjustmentParsedRecordsNonLotNumbered);
  log.audit(`${commentTag} => Adjustment Records`, adjustmentParsedRecords);
  return { adjustmentParsedRecords, lastExTime };
};

/**
 * Sort the data on keys and remove any fields with -None- key
 * @param {Object} mappedRecord
 * @returns {Array}
 */
const sortData = (mappedRecord) => Object.keys(mappedRecord).reduce((acc, obj) => {
  const innerArray = mappedRecord[obj];
  const tranformedArray = innerArray.map((innerObj) => {
    const toValues = Object.keys(innerObj)
      .sort((a, b) => a - b)
      .map((key) => { if (innerObj[key] === '- None -') { return ''; } return innerObj[key]; });
    return toValues;
  }, []);
  acc.push(tranformedArray);
  return acc;
}, []);

/**
 * Export Inventory Adjustments to Middleware
 * @returns {Array}
 */
const exportInventoryAdjustment = () => {
  const commentTag = 'InventoryAdjustment => exportInventoryAdjustment';
  try {
    // Extracting sync type i.e. INV ADJ
    syncType = SYNC_TYPE[syncFlow].INVENTORY_ADJUSTMENT;

    // Logging initialization of script
    LogHandler.init({ syncFlow, syncType });

    // Getting field mapping from Netsuite Custom Record
    /**
     * Here we have different mappings for Lot Numbered and Non Lot Numbered Items
     * due to reason that the filters were a bit complex and stacking both in single
     * saved search were not producing correct results
     */
    const fieldsMappingLotNumbered = RecordsFieldMapping.getIntegrationMappingRecord({
      recordType: FF_INTEGRATION_MAPPING.INVENTORY_ADJUSTMENT_LOT,
    });
    const fieldsMappingNonLotNumbered = RecordsFieldMapping.getIntegrationMappingRecord({
      recordType: FF_INTEGRATION_MAPPING.INVENTORY_ADJUSTMENT_NON_LOT,
    });

    log.audit(`${commentTag} => Fields Mapping Lot`, fieldsMappingLotNumbered);
    log.audit(`${commentTag} => fields Mapping Non Lot`, fieldsMappingNonLotNumbered);

    const { adjustmentParsedRecords, lastExTime } = getInventoryAdjustments(
      fieldsMappingLotNumbered,
      fieldsMappingNonLotNumbered,
    );

    const mappedRecord = getMappedRecord(fieldsMappingLotNumbered, adjustmentParsedRecords);

    log.audit(`${commentTag} => mappedRecord`, mappedRecord);

    sortData(mappedRecord);

    log.audit(`${commentTag} => mappedRecord and sorted`, mappedRecord);

    // Update the last sync time in Custom Record "Last Rec Sync"
    LogHandler.commit({
      lastSyncTime: lastExTime,
      status: LAST_RECORD_SYNC.SUCCESS,
      recordsProcessed: adjustmentParsedRecords.length,
      recordsSynched: mappedRecord.length,
      actionType: syncFlow,
    });
    return mappedRecord;
  } catch (error) {
    ErrorLog.recordNotCreated({
      messageDetails: `Error: ${error.name} : ${error.message}`,
      recordType: syncType,
      syncFlow,
      throwError: false,
    });
    return [];
  }
};

/**
 * Initialize NSCrud Object
 * @param {Object} BaseModule
 */
const init = (BaseModule) => {
  inventoryAdjustmentEntity = BaseModule;
  inventoryAdjustmentEntity.recordType = inventoryAdjustmentEntity
    .getRecordType().INVENTORY_ADJUSTMENT;
  SEARCH_OPERATOR = inventoryAdjustmentEntity.getSearchOperator();
  syncFlow = 'EXPORT';

  CONFIG.__proto__ = inventoryAdjustmentEntity.loadEnvironmentConfig(CONFIG_PATH);

  return {
    createAdjustment,
    exportInventoryAdjustment,
  };
};

export default init(new NSCrud.Entity());
