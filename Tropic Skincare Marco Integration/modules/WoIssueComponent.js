'use strict';

/**
 *
 * This file is responsible to parse WO Issue Components Data and create
 * Issue Component in NetSuite
 *
 * Version    Date                    Author           Remarks
 * 1.0.0      2019-12-19              Yasir
 *
 */

import log from 'N/log';
import _ from '../lib/lodash';
import moment from '../lib/moment';

import NSCrud from '../lib/f3_nscrud_base';
import globalConstants from '../globalConstants';
import Item from './Item';
import LogHandler from './LogHandler';
import ErrorLog from './ErrorLog';
import InventoryAdjustment from './InventoryAdjustment';
import InventoryNumber from './InventoryNumber';
import WorkOrder from './WorkOrders';

let workOrderIssueEntity;
let syncFlow;

const {
  SUBLIST,
  WORK_ORDER,
  WORK_ORDER_ISSUE,
  COMMON,
  CONFIG,
  CONFIG_PATH,
  LAST_RECORD_SYNC,
} = globalConstants;

/**
 * Check Data Mismatch
 * @param {Object} param0
 * @param {Array} ItemIds
 * @param {Array} dataToImport
 * @returns {Boolean}
 */
const checkDataMismatch = ({ recordReference, filename }, ItemIds = [], dataToImport = []) => {
  const isMismatch = (ItemIds.length !== dataToImport.length);
  if (isMismatch) {
    // Throwing error for Data Mismatch and logging ending details of script.
    LogHandler.commit({ lastSyncTimeCurrent: true });
    ErrorLog.dataMismatch({
      messageDetails: 'Data has some mismatch in netsuite',
      recordType: COMMON.WORK_ORDER_ISSUE,
      recordReference,
      syncFlow,
      filename,
      data: dataToImport,
      throwError: false,
    });
  }
  return isMismatch;
};

/**
 * Set inventory detail on Issue Component Record
 * @param {String} sublist
 * @param {Object} inventoryDetailSubrecord
 * @param {Object} data
 */
const setInventoryDetail = (sublist, inventoryDetailSubrecord, data = {}) => {
  inventoryDetailSubrecord.selectNewLine({
    sublistId: sublist,
  });

  for (const key in data) {
    inventoryDetailSubrecord.setCurrentSublistValue({
      sublistId: sublist,
      fieldId: key,
      value: data[key],
    });
  }

  inventoryDetailSubrecord.commitLine({
    sublistId: sublist,
  });
};

/**
 * Set Line Values of Issue Component
 * @param {Object} woIssueObj
 * @param {Object} dataToImport
 */
const setWOLineAndInventory = (woIssueObj, dataToImport) => {
  woIssueObj.setCurrentSublistValue({
    sublistId: SUBLIST.COMPONENT,
    fieldId: WORK_ORDER_ISSUE.QUANTITY,
    value: dataToImport.quantity,
  });

  // Checking if Item is lot numbered or not
  const isLotNumbered = woIssueObj.getCurrentSublistValue({
    sublistId: SUBLIST.COMPONENT,
    fieldId: WORK_ORDER_ISSUE.INVENTORY_DETAIL.COMPONENT_INVENTORY_DETAIL_AVAILABLE,
  });

  if (isLotNumbered === COMMON.IS_TRUE) {
    const inventoryDetailSubrecord = woIssueObj.getCurrentSublistSubrecord({
      sublistId: SUBLIST.COMPONENT,
      fieldId: WORK_ORDER_ISSUE.INVENTORY_DETAIL.COMPONENT_INVENTORY_DETAIL,
    });
    setInventoryDetail(SUBLIST.INVENTORY_ASSIGNMENT, inventoryDetailSubrecord, {
      [WORK_ORDER_ISSUE.INVENTORY_DETAIL.ISSUE_INVENTORY_NUMBER]: dataToImport.lotnumber,
      [WORK_ORDER_ISSUE.QUANTITY]: dataToImport.quantity,
    });
  }
};

/**
 * Transform Work order to issue component
 * @param {String} workOrderId
 * @returns {Object}
 */
const transfromToIssueComponent = (workOrderId) => workOrderIssueEntity.transform({
  fromType: WORK_ORDER.Type,
  fromId: workOrderId,
  toType: WORK_ORDER_ISSUE.Type,
  isDynamic: true,
});

/**
 * Set Line Item details of Issue Component
 * @param {Object} woIssueObj
 * @param {Array} dataToImport
 */
const setLineValues = (woIssueObj, dataToImport) => {
  const commentTag = 'WoIssueComponent => setLineValues';
  // Set Line and Inventory Detail values of item
  const lines = woIssueObj.getLineCount({ sublistId: SUBLIST.COMPONENT });

  // Looping over each line to set details
  for (let index = 0; index < lines; index += 1) {
    woIssueObj.selectLine({
      sublistId: SUBLIST.COMPONENT,
      line: index,
    });

    // Getting Item id to match with import data
    const woItem = woIssueObj.getCurrentSublistValue({
      sublistId: SUBLIST.COMPONENT,
      fieldId: WORK_ORDER_ISSUE.ITEM,
    });

    log.debug(`${commentTag} => Data To Import`, dataToImport);
    log.debug(`${commentTag} => WO Item`, woItem);

    // Comparing Available and Issued Quantity.
    // If Available < Issued then we have to make a inventory adjustment
    // Here parseFloat is used instead of parseInt on Quantity because
    // quantity can be issued in decimal numbers like 2.35
    if (woItem === dataToImport.item) {
      if (parseFloat(dataToImport.available) < parseFloat(dataToImport.quantity)) {
        // Calculating difference in Quantity to create Adjustment
        const quantityDifference = dataToImport.quantity - dataToImport.available;
        InventoryAdjustment.createAdjustment(quantityDifference, dataToImport);
      }

      // Setting Remaining Line Values
      setWOLineAndInventory(woIssueObj, dataToImport);
    } else {
      // Setting Quantity as 0 so that it isn't issued
      woIssueObj.setCurrentSublistValue({
        sublistId: SUBLIST.COMPONENT,
        fieldId: WORK_ORDER_ISSUE.QUANTITY,
        value: 0,
      });
    }
    woIssueObj.commitLine({
      sublistId: SUBLIST.COMPONENT,
    });
  }
};

/**
 * Create Issue Component from Work Order
 * Also Creates and Inventory Adjustment if
 * available quantity of lot is less than issued
 * @param {Array} dataToImport
 * @param {String} workOrderId
 * @returns {String}
 */
const issueComponents = (dataToImport, workOrderId) => {
  // Transforming Work Order to Issue Component
  const woIssueObj = transfromToIssueComponent(workOrderId);

  setLineValues(woIssueObj, dataToImport);

  return workOrderIssueEntity.saveRecord(woIssueObj);
};

/**
 * Search For Lot Number of Item on defined Location
 * @param {Array} dataToImport
 * @param {String} itemId
 * @returns {Array}
 */
const getLotNumber = (dataToImport, itemId) => InventoryNumber.getInternalId(_.compact(_.map(
  [dataToImport],
  COMMON.LOT_NUMBER,
)), itemId, CONFIG.__proto__.LOCATIONS.MARCO_LOCATION);

/**
 * Search for Items in Netsuite
 * @param {Array} dataToImport
 * @param {Object} param1
 * @returns {Object}
 */
const verifyItemsInNetsuite = (dataToImport, { recordReference, filename }) => {
  const commentTag = 'WoIssueComponent => verifyItemsInNetsuite';

  // Extracting all Items SKU to get internal Id's in netsuite.
  const itemCodes = _.compact(_.map([dataToImport], COMMON.ITEM));
  log.debug(`${commentTag} => Item Codes External System`, itemCodes);

  // Searching Items in Netsuite
  const itemIds = Item.getInternalId(itemCodes);
  log.debug(`${commentTag} => Items in Netsuite`, itemIds);

  const misMatch = checkDataMismatch(
    { recordReference, filename },
    _.uniq(itemCodes),
    itemIds,
  );

  // Checking for any non existing Item in Netsuite.
  return {
    misMatch,
    itemIds,
  };
};

/**
 * Prepare Data and Create Work Order Issue in NetSuite
 * @param {Object} context
 * @returns {String}
 */
const createWorkOrderIssue = (context) => {
  const commentTag = 'WoIssueComponent => createWorkOrderIssue';
  const {
    recordReference,
    dataToImport,
    filename,
  } = context;

  try {
    const { misMatch, itemIds } = verifyItemsInNetsuite(
      dataToImport,
      { recordReference, filename },
    );
    if (misMatch) {
      return true;
    }

    log.audit(`${commentTag} => WO Issue Data`, dataToImport);

    // Extracting Work Order Id's and Searching in NetSuite
    const workOrderId = WorkOrder.getWorkOrderId(_.compact(_.map([dataToImport], COMMON.TRAN_ID)));
    log.audit(`${commentTag} => WorkOrder Id`, workOrderId);

    // Throwing Error if Work Order Does Not Exist
    if (workOrderId && workOrderId.length === 0) {
      const errorObj = { name: 'Record not found', message: 'Cannot find work order specified' };
      throw errorObj;
    }

    // Search for Lot Number of Item in Issue Component
    // Here we used just first Index of Item Id as each
    // Issue Component contain only one item. Meaning this
    // function is invoked only for single item
    const itemNumber = getLotNumber(dataToImport, itemIds[0].internalid);
    log.audit(`${commentTag} => Lot Number`, itemNumber);

    // Parsing Date Object according to NetSuite
    const trandate = workOrderIssueEntity.parseToValue({
      value: (dataToImport.trandate && moment(
        dataToImport.trandate,
        COMMON.RECEIVED_DATE_FORMAT,
      ).format(COMMON.DATE_FORMAT)) || moment().toDate(),
      type: 'datetime',
      timezone: COMMON.TIME_ZONE,
    });

    // Assigning all the values to import object
    _.assign(dataToImport, {
      trandate,
      item: itemIds[0].internalid,
      lotnumber_text: dataToImport.lotnumber,
      lotnumber: itemNumber && itemNumber.length > 0 ? itemNumber[0].internalid : 0,
      available: itemNumber && itemNumber.length > 0 ? itemNumber[0].quantityavailable : 0,
    });

    // Issue Component From Work Order
    const issueComponentId = issueComponents(dataToImport, workOrderId[0].internalid);

    return issueComponentId;
  } catch (error) {
    LogHandler.recordFailed();
    ErrorLog.recordNotCreated({
      messageDetails: `Error: ${error.name} : ${error.message}`,
      recordType: COMMON.WORK_ORDER_ISSUE,
      syncFlow,
      throwError: false,
      recordReference,
      filename,
    });
  }
};

/**
 * Extract Work Order Issue Data
 * @param {Object} context
 * @returns {Array}
 */
const importWorkOrderIssue = (context) => {
  // Logging start time of script
  workOrderIssueEntity.scriptStartTime();

  // Initializing Script log details. This is a custom record which logs all
  // the details like Script Start time, end time, Total Records Processed, etc.
  LogHandler.init({ syncFlow, syncType: COMMON.WORK_ORDER_ISSUE });
  const { data = [], filename = 'filename missing' } = context;

  const importedWO = [];

  // Looping over each WO Issue Record
  for (let i = 0; i < data.length; i += 1) {
    // Creating Data Object to be Imported
    const dataToImport = data[i];
    const recordReference = dataToImport.tranid;

    // Creating Data Object for Work Order Issue Component creation
    const dataObject = { recordReference, dataToImport, filename };
    importedWO.push(createWorkOrderIssue(dataObject));
  }

  LogHandler.commit({
    lastSyncTimeCurrent: true,
    status: LAST_RECORD_SYNC.SUCCESS,
    actionType: syncFlow,
  });
  return importedWO;
};

/**
 * Initialize NSCrud Object
 * @param {Object} BaseModule
 * @returns {Object}
 */
const init = (BaseModule) => {
  workOrderIssueEntity = BaseModule;
  workOrderIssueEntity.recordType = workOrderIssueEntity.getRecordType().WORK_ORDER_ISSUE;
  syncFlow = 'IMPORT';

  CONFIG.__proto__ = workOrderIssueEntity.loadEnvironmentConfig(CONFIG_PATH);

  return {
    importWorkOrderIssue,
    createWorkOrderIssue,
  };
};

export default init(new NSCrud.Entity());
