'use strict';

/**
 *
 * This file is responsible to Search for Work Orders after last sync time
 * and Export them as CSV to Middleware
 *
 * Version    Date                    Author           Remarks
 * 1.0.0      2019-12-19              Yasir
 *
 */

import log from 'N/log';
import _ from '../lib/lodash';

import NSCrud from '../lib/f3_nscrud_base';
import globalConstants from '../globalConstants';
import RecordsFieldMapping from './RecordsFieldMapping';
import LastRecordSync from './LastRecordSync';
import ErrorLog from './ErrorLog';
import LogHandler from './LogHandler';

const {
  WORK_ORDER,
  FF_INTEGRATION_MAPPING,
  COMMON,
  SYNC_TYPE,
  LAST_RECORD_SYNC,
} = globalConstants;

const dateFormat = 'DD.MM.YYYY HH12:MI:SS AM';

const DEFAULT_COLUMNS = [
  { name: WORK_ORDER.UPDATED_DATE, sort: COMMON.SORT.ASC },
  { name: 'formulatext', formula: `TO_CHAR({${WORK_ORDER.UPDATED_DATE}}, '${dateFormat}')` },
  { name: COMMON.INTERNAL_ID, type: COMMON.ByValue },
];

const searchParams = {
  columns: undefined,
  filters: undefined,
  type: undefined,
};

let workOrderEntity;
let syncFlow;
let syncType;
let SEARCH_OPERATOR;

/**
 * Execute and Extract results of Saved Search
 * @param {Object} context
 * @returns {Array}
 */
const executeSearch = (context = {}) => {
  try {
    const woSearch = workOrderEntity.search(context);
    const woRecords = workOrderEntity.getAllRecords(woSearch);
    const woParsedRecords = workOrderEntity.parseSearchResults(woRecords);
    return woParsedRecords;
  } catch (error) {
    // Logging Script Ending logs
    LogHandler.commit();

    // Updating Sync Time to current Time
    LastRecordSync.updateSyncTime({
      recordType: syncType,
    });

    // Throwing Error in Custom Record which will be emailed
    ErrorLog.searchNotExecuted({
      messageDetails: error.name, recordType: syncType, syncFlow,
    });
  }
};

/**
 * Search for Work Order Id's using codes
 * @param {String} woCodes
 * @returns {Array}
 */
const getWorkOrderId = (woCodes) => {
  workOrderEntity.columns = [
    { name: COMMON.INTERNAL_ID, type: COMMON.ByValue },
  ];

  const filters = [
    [WORK_ORDER.TRAN_ID, SEARCH_OPERATOR.IS, woCodes],
    'AND',
    [COMMON.MAIN_LINE, SEARCH_OPERATOR.IS, COMMON.IS_TRUE],
  ];
  return _.map(executeSearch({ filters }), (value) => ({
    [COMMON.INTERNAL_ID]: value[COMMON.INTERNAL_ID],
  }));
};

/**
 * Create Formula Filter for Last Time Execution till current time
 * @param {String} lastSyncTime
 */
const applyUpdateDateFilter = (lastSyncTime) => {
  if (lastSyncTime) {
    // Creating Formula to Run Saved search after last sync time
    const formulaExp = `formulanumeric: ADD_MONTHS({${WORK_ORDER.UPDATED_DATE}}, 0)-ADD_MONTHS(TO_DATE('${lastSyncTime}', 'DD.MM.YYYY HH12:MI:SS AM'), 0)`;
    const updatedDateFilter = [
      'AND',
      [formulaExp, SEARCH_OPERATOR.GREATERTHAN, '0'],
    ];

    // Appending Time Filter
    searchParams.filters.push(...updatedDateFilter);
  }
};

/**
 * Return Last Sync Run time from Custom Record in NetSuite
 * @param {String} recordType
 * @returns {String}
 */
const getLastSyncTime = (recordType) => {
  const { syncTime } = LastRecordSync.getSyncTime({ recordType });
  return syncTime;
};

/**
 * Create Columns and Filter for Saved Search
 * @param {Array} fieldsMapping
 */
const createColumnsAndFilters = (fieldsMapping) => {
  // Creating Columns of fields mapping data
  const COLUMNS = RecordsFieldMapping.netSuiteSearchColumnFromMapping(fieldsMapping);

  // Appending Fields Mapping Columns (can be changed in Custom Record) and
  // Default Columns (Should not be changed)
  workOrderEntity.columns = [...COLUMNS, ...DEFAULT_COLUMNS];
  searchParams.filters = [
    [COMMON.TAX_LINE, SEARCH_OPERATOR.IS, COMMON.IS_FALSE],
    'AND',
    [COMMON.COGS_LINE, SEARCH_OPERATOR.IS, COMMON.IS_FALSE],
    'AND',
    [COMMON.SHIPPING_LINE, SEARCH_OPERATOR.IS, COMMON.IS_FALSE],
    'AND',
    [`formulanumeric: {${WORK_ORDER.QUANTITY}}`, SEARCH_OPERATOR.GREATERTHAN, '0'],
    'AND',
    [COMMON.STATUS, SEARCH_OPERATOR.ANYOF, WORK_ORDER.STATUS_CANCELLED, WORK_ORDER.STATUS_RELEASED,
      WORK_ORDER.STATUS_IN_PROCESS_WORK_ORDER],
    'AND',
    [[WORK_ORDER.MARCO_INGREDIENT, SEARCH_OPERATOR.IS, COMMON.IS_TRUE],
      'OR',
      [COMMON.LOCATION, SEARCH_OPERATOR.ANYOF, COMMON.LOCATION_LONDON_HQ]],
  ];
};

/**
 * Delete the property from object
 * @param {Array} mappedRecord
 * @param {String} propertyToDelete
 * @returns {Array}
 */
const deleteProperty = (mappedRecord, propertyToDelete) => {
  // We need two nested Map here because data was grouped in array of array format
  _.map(
    _.values(mappedRecord),
    (mappedRecordObj) => _.map(mappedRecordObj, (obj) => delete obj[propertyToDelete]),
  );
  return mappedRecord;
};

/**
 * Create and Execute Search for Work Orders
 * @param {Array} fieldsMapping
 * @returns {Array}
 */
const getWorkOrders = (fieldsMapping) => {
  // Create Columns and Filters for workorders search
  createColumnsAndFilters(fieldsMapping);

  searchParams.type = workOrderEntity.getSearchType().WORK_ORDER;

  // Apply date filter from Custom Record "Last Rec Sync"
  // This function get's Last sync time from a custom record and create a
  // filter so that the records from last export cycle only comes in saved search.
  applyUpdateDateFilter(getLastSyncTime(syncType));

  return executeSearch(searchParams);
};

/**
 * Replace Sequence Number in Work Order Results
 * @param {Array} mappedRecord
 * @param {String} sequenceNumberIndex
 * @returns {Array}
 */
const appendSequenceNumber = (mappedRecord, sequenceNumberIndex) => _.map(
  mappedRecord,
  (obj) => _.map(
    obj,
    (innerObj, index) => { innerObj[sequenceNumberIndex] = index; },
  ),
);

/**
 * Split Work Order Header and Line Results
 * For Example: The record contains 16 lines i.e. 8 header and 8 line
 * Below code will map over the record and extract single work order record
 * (since the records are grouped by work order itnernal id)
 * On the work order record it will map and extract the single record from it
 * i.e. the item fields. On each item field's it will split them based on their
 * type i.e Header or line based on their index.
 * @param {Array} mappedRecord
 * @param {String} headerCount
 * @returns {Array}
 */
const splitData = (mappedRecord, headerCount) => _.map(
  _.values(mappedRecord),
  (rec) => _.map(
    rec,
    (singleRec, index) => _.filter(
      singleRec,
      (value, i) => (index === 0 ? (i >= 0 && i <= headerCount) : (i > headerCount)),
    ),
  ),
);

/**
 * Map Work Orders Result to External System Format
 * @param {Array} fieldsMapping
 * @param {Array} woParsedRecords
 * @param {String} headerCount
 * @returns {Array}
 */
const getMappedRecord = (fieldsMapping, woParsedRecords, headerCount) => {
  const commentTag = 'WorkOrders => getMappedRecord';

  try {
    // Mapping Searched Fields according to External System
    let mappedRecord = RecordsFieldMapping.mapRemaingFields(
      woParsedRecords,
      fieldsMapping,
      { recordType: syncType, syncFlow },
    );

    log.audit(`${commentTag} => Mapped WorkOrders`, mappedRecord);

    // Getting Index of internal id for grouping record and key deletion
    const internalIdIndex = _.find(fieldsMapping, { nsField: WORK_ORDER.INTERNAL_ID }).ffField;

    // Getting Sequence Number Index in order to relace it with our own sequencing.
    // We need this replacement as the data is grouped and sequencing does not remain correct.
    const sequenceNumberIndex = _.find(
      fieldsMapping,
      { nsField: WORK_ORDER.LINE_SEQUENCE_NUMBER },
    ).ffField;

    log.debug(`${commentTag} => sequenceNumberIndex`, sequenceNumberIndex);

    // Grouping Records on Internal Id
    mappedRecord = _.groupBy(mappedRecord, internalIdIndex);

    // Delete Internal Id Property
    mappedRecord = deleteProperty(mappedRecord, internalIdIndex);

    // Append Sequence Number Manually
    // Here we needed nested Map as mentioned before the data is
    // in format of array of array
    appendSequenceNumber(mappedRecord, sequenceNumberIndex);

    // Below line is to split the header and line fields from the record returned by netsuite
    splitData(mappedRecord, headerCount);

    mappedRecord = log.audit(`${commentTag} => mapped record`, mappedRecord);
    return mappedRecord;
  } catch (error) {
    ErrorLog.recordNotCreated({
      messageDetails: `Error: ${error.name} : ${error.message}`,
      recordType: syncType,
      syncFlow,
      throwError: false,
    });
  }
};

/**
 * Export all work orders created after last time sync
 * @returns {Array}
 */
const exportWorkOrder = () => {
  const commentTag = 'WorkOrders => exportWorkOrder';
  try {
    syncType = SYNC_TYPE[syncFlow].WORK_ORDER;

    // Initializing Script log details. This is a custom record which logs all
    // the details like Script Start time, end time, Total Records Processed, etc.
    LogHandler.init({ syncFlow, syncType });

    // Getting field mapping from Netsuite Custom Record
    const fieldsMapping = RecordsFieldMapping.getIntegrationMappingRecord({
      recordType: FF_INTEGRATION_MAPPING.WORK_ORDER,
    });

    // Getting count of indexes (Header and Line) to split
    const headerCount = (_.groupBy(fieldsMapping, WORK_ORDER.FIELD_MAPPING_TYPE).Header.length) - 1;

    // Search for Work Orders
    const woParsedRecords = getWorkOrders(fieldsMapping);

    log.audit(`${commentTag} => Work Orders`, woParsedRecords);

    // Map Workorders data according to external system requirement
    const mappedRecord = getMappedRecord(fieldsMapping, woParsedRecords, headerCount);

    log.audit('WorkOrders => exportWorkOrder => Mapped Record', mappedRecord);

    const { formulatext = '' } = _.last(woParsedRecords) || [];

    // Update the last sync time in Custom Record "Last Rec Sync"
    LastRecordSync.updateSyncTime({
      recordType: syncType,
      lastSyncTime: formulatext,
      status: LAST_RECORD_SYNC.SUCCESS,
      actionType: syncFlow,
    });
    LogHandler.commit({
      lastSyncTime: formulatext,
      status: LAST_RECORD_SYNC.SUCCESS,
      recordsProcessed: mappedRecord.length,
      recordsSynched: mappedRecord.length,
      actionType: syncFlow,
    });
    return mappedRecord;
  } catch (error) {
    ErrorLog.recordNotCreated({
      messageDetails: `Error: ${error.name} : ${error.message}`,
      recordType: syncType,
      syncFlow,
      throwError: false,
    });
  }
};

/**
 * Initialize NSCrud Object
 * @param {Array} baseModule
 * @returns {Object}
 */
const init = (baseModule) => {
  workOrderEntity = baseModule;
  workOrderEntity.recordType = workOrderEntity.getRecordType().WORK_ORDER;
  syncFlow = 'EXPORT';
  SEARCH_OPERATOR = workOrderEntity.getSearchOperator();
  return {
    exportWorkOrder,
    getWorkOrderId,
  };
};

export default init(new NSCrud.Entity());
